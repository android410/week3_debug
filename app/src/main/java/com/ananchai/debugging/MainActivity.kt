package com.ananchai.debugging

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"this is where the app crashed before")
        val helloTextView:TextView = findViewById(R.id.hello_world)
        Log.d(TAG,"this should be logged if the bug is fixed")
        helloTextView.text = "Hello, debugging"
        setContentView(R.layout.activity_main)
        logging()
        division()
    }

    private fun division() {
        val numeratour = 60
        var denominator = 4
        repeat(5){
            Log.d(TAG,"$denominator")
            Log.v(TAG, "${numeratour /denominator}")
            denominator--
        }
    }

    private fun logging(){
        Log.e(TAG, "Hello, world!")
        Log.w(TAG, "Hello, world!")
        Log.i(TAG, "Hello, world!")
        Log.d(TAG, "Hello, world!")
        Log.v(TAG, "Hello, world!")
    }
    companion object {
        private const val TAG = "MainActivity"
    }
}